// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD-qPH6PoI76pdoFH7j3pa7g6my8lmueYI",
    authDomain: "hello-chirelle.firebaseapp.com",
    databaseURL: "https://hello-chirelle.firebaseio.com",
    projectId: "hello-chirelle",
    storageBucket: "hello-chirelle.appspot.com",
    messagingSenderId: "141183788646",
    appId: "1:141183788646:web:a690d37e5e97f24a806fa7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
